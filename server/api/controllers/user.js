import {Router} from 'express';
import User from "../models/User";
import {Op} from 'sequelize';

const db = require('../../config/database');

const router = Router();


// const Sequelize = require('sequelize');
// const Op = Sequelize.Op;

router.get('/user/all', (req, res) =>
  User.findAll()
    .then(user => {
        res.status(200).json(user)
      }
    ).catch(err => console.log(err)));

/* GET users listing. */
// router.get('/user', function (req, res, next) {
//   userModel.getUsers((err, data) => {
//     res.status(200).json(data);
//   });
//
// })


export default router;
