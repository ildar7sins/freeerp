import { Router } from 'express';

//import users from './controllers/users/users';
import user from './controllers/user';

const router = Router();

// Add USERS Routes
//router.use(users);
router.use(user);

export default router;
