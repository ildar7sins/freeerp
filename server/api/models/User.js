const Sequelize = require('sequelize');
const db = require('../../config/database');


const User = db.define('user', {
  username: {
    type: Sequelize.STRING
  },
  vk_id: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  },
  status: {
    type: Sequelize.INTEGER
  },
  created_at: {
    type: Sequelize.INTEGER
  },
  updated_at: {
    type: Sequelize.INTEGER
  },
  logged_at: {
    type: Sequelize.INTEGER
  }
}, {
  tableName: 'user'
});

module.exports = User;
